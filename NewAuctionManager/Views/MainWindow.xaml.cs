﻿using NewAuctionManager.ViewModels;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace NewAuctionManager
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public AuctionsViewModel aucs = new AuctionsViewModel();

        public bool IsEditingAuctionNumber { get; set; } = false;

        public MainWindow()
        {
            InitializeComponent();            
            DataContext = aucs;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            IsEditingAuctionNumber = !IsEditingAuctionNumber;
            openButton.GetBindingExpression(Button.VisibilityProperty).UpdateTarget();
            goButton.GetBindingExpression(Button.VisibilityProperty).UpdateTarget();
            auctionNumber.GetBindingExpression(TextBox.VisibilityProperty).UpdateTarget();
            if (IsEditingAuctionNumber)
                auctionNumber.Text = "";
        }
    }
}
