﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using NewAuctionManager.Commands;
using System.Threading.Tasks;
using ToastNotifications;
using System.Windows.Threading;

namespace NewAuctionManager.ViewModels
{
    public class AuctionsViewModel : BaseViewModel
    {
        public ObservableCollection<AuctionViewModel> ActiveAuctions { get; } = new ObservableCollection<AuctionViewModel>();

        private AuctionViewModel _selectedAuction;
        public AuctionViewModel SelectedAuction
        {
            get { return _selectedAuction; }
            set
            {
                if (_selectedAuction == value) return;
                _selectedAuction = value;
                OnPropertyChanged("SelectedAuction");
            }
        }

        private NotificationsSource _notificationSource = new NotificationsSource();

        public NotificationsSource NotificationSource
        {
            get { return _notificationSource; }
            set
            {
                _notificationSource = value;
                OnPropertyChanged("NotificationSource");
            }
        }

        private int _updateProgress;
        public int UpdateProgress {
            get { return _updateProgress; }
            set
            {
                _updateProgress = value;
                OnPropertyChanged("UpdateProgress");
            }
        }

        private bool _isUpdating;
        public bool IsUpdating
        {
            get { return _isUpdating; }
            set
            {
                _isUpdating = value;
                OnPropertyChanged("IsUpdating");
            }
        }

        //*** ASYNC COMMANDS ***
        public ICommand UpdateCommand { get; set; }
        //*** ASYNC COMMANDS ***

        private CommandHandler _openLinkCommand;

        public ICommand OpenLinkCommand
        {
            get
            {
                return _openLinkCommand ?? (_openLinkCommand = new CommandHandler(param => ExecuteOpenLink(param), (o) => true));
            }
        }


        private CommandHandler _updateNameCommand;

        public ICommand UpdateNameCommand
        {
            get
            {
                return _updateNameCommand ?? (_updateNameCommand = new CommandHandler(param =>
                {
                    _selectedAuction.Name = param.ToString();
                    DataManager.UpdateAuction(_selectedAuction.Model);
                    ShowToast("Название аукциона обновлено", NotificationType.Success);
                }, (o) => true));
            }
        }

        private CommandHandler _updateIsInUseCommand;

        public ICommand UpdateIsInUseCommand
        {
            get
            {
                return _updateIsInUseCommand ?? (_updateIsInUseCommand = new CommandHandler(param =>
                {
                    DataManager.UpdateAuction(_selectedAuction.Model);
                    if (_selectedAuction.IsInUse)
                    {
                        ActiveAuctions.Add(_selectedAuction);
                        ShowToast(_selectedAuction.UID + " добавлен в список отслеживаемых", NotificationType.Success);
                    }
                    else
                    {
                        ShowToast(_selectedAuction.UID + " удален из списка отслеживаемых", NotificationType.Success);
                        ActiveAuctions.Remove(_selectedAuction);
                    }
                }, (o) => true));
            }
        }

        private CommandHandler _searchAndOpenAuctionCommand;

        public ICommand SearchAndOpenAuctionCommand
        {
            get
            {
                return _searchAndOpenAuctionCommand ?? (_searchAndOpenAuctionCommand = new CommandHandler(param =>
                {
                    Task.Run(() =>
                    {
                        string str = param as string;
                        if (String.IsNullOrWhiteSpace(str))
                        {
                            ShowToast("Введите номер аукциона!", NotificationType.Warning);                        
                            return;
                        }
                        ShowToast("Загрузка аукциона " + str, NotificationType.Information);
                        var auc = DataManager.GetAuctionById(str);
                        if (auc == null)
                        {
                            ShowToast("Не удалось загрузить аукцион " + str, NotificationType.Error);
                            return;
                        }
                        var aucvm = new AuctionViewModel(auc);
                        SelectedAuction = aucvm;
                        ShowToast("Аукцион " + str + " загружен", NotificationType.Success);
                    });
                }, (o) => true));
            }
        }

        private void ShowToast(string message, NotificationType type = NotificationType.Information)
        {
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                          new Action(() => NotificationSource.Show(message, type)));
        }

        private void ExecuteOpenLink(object param)
        {
            string link = param as string;
            if (link == null)
                return;
            System.Diagnostics.Process.Start(link);
        }

        public AuctionsViewModel()
        {
            LoadAuctionsFromDB();

            var prog = new Progress<int>();
            prog.ProgressChanged += Prog_ProgressChanged;
            UpdateCommand = new Commands.AsyncCommand(async () =>
            {
               await Task.Run(() =>
               {
                   ShowToast("Обновление базы данных", NotificationType.Information);
                   IsUpdating = true;
                   DataManager.InitDatabase();
                   DataManager.UpdateDatabase(prog);
                   IsUpdating = false;
                   ShowToast("База данных обновлена", NotificationType.Success);
               });
            });
        }

        private void Prog_ProgressChanged(object sender, int e)
        {
            UpdateProgress = e;
        }

        private void LoadAuctionsFromDB()
        {
            var auctions = DataManager.GetActiveAuctions();
            foreach (var auctionModel in auctions)
            {
                ActiveAuctions.Add(new AuctionViewModel(auctionModel));
            }
        }
    }
}
