﻿using System;
using System.Collections.ObjectModel;
using NewAuctionManager.Models;

namespace NewAuctionManager.ViewModels
{
    public class AuctionViewModel : BaseViewModel
    {
        private AuctionModel _model;

        public AuctionModel Model { get {return _model;} }

        public AuctionViewModel(AuctionModel model)
        {
            _model = model;
        }

        public string Name
        {
            get { return _model?.Name; }
            set { if (_model == null) return; _model.Name = value; OnPropertyChanged("Name"); }
        }
        public string UID
        {
            get { return _model?.UID; }
            set { if (_model == null) return; _model.UID = value; OnPropertyChanged("UID"); }
        }
        public string Content
        {
            get { return _model?.Content; }
            set { if (_model == null) return; _model.Content = value; OnPropertyChanged("Content"); }
        }
        public DateTime PlacedDate
        {
            get { if (_model == null) return DateTime.Now; else return _model.PlacedDate; }
            set { if (_model == null) return; _model.PlacedDate = value; OnPropertyChanged("PlacedDate"); }
        }
        public DateTime AcceptFirstPartBeforeDateTime
        {
            get { if (_model == null) return DateTime.Now; else return _model.AcceptFirstPartBeforeDateTime; }
            set { if (_model == null) return; _model.AcceptFirstPartBeforeDateTime = value; OnPropertyChanged("AcceptFirstPartBeforeDateTime"); }
        }
        public DateTime FirstPartCheckDate
        {
            get { if (_model == null) return DateTime.Now; else return _model.FirstPartCheckDate; }
            set { if (_model == null) return; _model.FirstPartCheckDate = value; OnPropertyChanged("FirstPartCheckDate"); }
        }
        public DateTime AuctionDateTime
        {
            get { if (_model == null) return DateTime.Now; else return _model.AuctionDateTime; }
            set { if (_model == null) return; _model.AuctionDateTime = value; OnPropertyChanged("AuctionDateTime"); }
        }
        public double Price
        {
            get { if (_model == null) return 0.0; else return _model.Price; }
            set { if (_model == null) return; _model.Price = value; OnPropertyChanged("Price"); }
        }
        public double ApplicationGuarantee
        {
            get { if (_model == null) return 0.0; else return _model.ApplicationGuarantee; }
            set { if (_model == null) return; _model.ApplicationGuarantee = value; OnPropertyChanged("ApplicationGuarantee"); }
        }
        public double ContractGuarantee
        {
            get { if (_model == null) return 0.0; else return _model.ContractGuarantee; }
            set { if (_model == null) return; _model.ContractGuarantee = value; OnPropertyChanged("ContractGuarantee"); }
        }
        public string PlaceOfDelivery
        {
            get { return _model?.PlaceOfDelivery; }
            set { if (_model == null) return; _model.PlaceOfDelivery = value; OnPropertyChanged("PlaceOfDelivery"); }
        }
        public string AuctionOwner
        {
            get { return _model?.AuctionOwner; }
            set { if (_model == null) return; _model.AuctionOwner = value; OnPropertyChanged("AuctionOwner"); }
        }
        public bool IsInUse
        {
            get { if (_model == null) return false; else return _model.IsInUse; }
            set { if (_model == null) return; _model.IsInUse = value; OnPropertyChanged("IsInUse"); }
        }
        public bool IsCancelled
        {
            get { if (_model == null) return false; else return _model.IsCancelled; }
            set { if (_model == null) return; _model.IsCancelled = value; OnPropertyChanged("IsCancelled"); }
        }
        public DateTime LastUpdateDateTime
        {
            get { if (_model == null) return DateTime.Now; else return _model.LastUpdateDateTime; }
            set { if (_model == null) return; _model.LastUpdateDateTime = value; OnPropertyChanged("LastUpdateDateTime"); }
        }
        public ObservableCollection<TenderNotificationModel> Notifications
        {
            get { return _model?.Notifications; }
            set { if (_model == null) return; _model.Notifications = value; OnPropertyChanged("Notifications"); }
        }
        public string Link
        {
            get { return _model?.Link; }
            set { if (_model == null) return; _model.Link = value; OnPropertyChanged("Link"); }
        }
        public ObservableCollection<DocumentModel> Documents
        {
            get { return _model?.Documents; }
            set { if (_model == null) return; _model.Documents = value; OnPropertyChanged("Documents"); }
        }
        public ObservableCollection<LocalDocumentModel> LocalDocuments
        {
            get { return _model?.LocalDocuments; }
            set { if (_model == null) return; _model.LocalDocuments = value; OnPropertyChanged("LocalDocuments"); }
        }
        public ObservableCollection<ClarificationModel> Clarifications
        {
            get { return _model?.Clarifications; }
            set { if (_model == null) return; _model.Clarifications = value; OnPropertyChanged("Clarifications"); }
        }
    }
}
