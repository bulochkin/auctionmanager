﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace NewAuctionManager.UserControls
{
    

    public partial class AuctionView : UserControl
    {
        public static readonly DependencyProperty OpenLinkCommandProperty =
        DependencyProperty.Register(
            "OpenLinkCommand",
            typeof(ICommand),
            typeof(AuctionView),
            new UIPropertyMetadata(null));

        public static readonly DependencyProperty UpdateNameCommandProperty = 
        DependencyProperty.Register(
            "UpdateNameCommand", 
            typeof(ICommand), 
            typeof(AuctionView), 
            new PropertyMetadata(default(ICommand)));

        public static readonly DependencyProperty UpdateIsInUseCommandProperty =
        DependencyProperty.Register(
            "UpdateIsInUseCommand",
            typeof(ICommand),
            typeof(AuctionView),
            new PropertyMetadata(default(ICommand)));

        public ICommand OpenLinkCommand
        {
            get { return (ICommand)GetValue(OpenLinkCommandProperty); }
            set { SetValue(OpenLinkCommandProperty, value); }
        }

        public ICommand UpdateNameCommand
        {
            get { return (ICommand) GetValue(UpdateNameCommandProperty); }
            set { SetValue(UpdateNameCommandProperty, value); }
        }

        public ICommand UpdateIsInUseCommand
        {
            get { return (ICommand)GetValue(UpdateIsInUseCommandProperty); }
            set { SetValue(UpdateIsInUseCommandProperty, value); }
        }

        public AuctionView()
        {
            InitializeComponent();
        }
    }
}
