﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NewAuctionManager.UserControls
{
    /// <summary>
    /// Логика взаимодействия для EditableLabeledTextLine.xaml
    /// </summary>
    public partial class EditableLabeledTextLine : UserControl
    {
        public static readonly DependencyProperty LabelProperty =
        DependencyProperty.Register(
            "Label",
            typeof(string),
            typeof(EditableLabeledTextLine),
            new UIPropertyMetadata(null));

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        public static readonly DependencyProperty IsEditingProperty =
        DependencyProperty.Register(
            "IsEditing",
            typeof(bool),
            typeof(EditableLabeledTextLine),
            new UIPropertyMetadata(false));

        public bool IsEditing
        {
            get { return (bool)GetValue(IsEditingProperty); }
            set { SetValue(IsEditingProperty, value); }
        }

        public static readonly DependencyProperty IsEditableProperty =
        DependencyProperty.Register(
            "IsEditable",
            typeof(bool),
            typeof(EditableLabeledTextLine),
            new UIPropertyMetadata(false));

        public bool IsEditable
        {
            get { return (bool)GetValue(IsEditableProperty); }
            set { SetValue(IsEditableProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
        DependencyProperty.Register(
            "Text",
            typeof(string),
            typeof(EditableLabeledTextLine),
            new UIPropertyMetadata(null));

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty UpdateCommandProperty =
        DependencyProperty.Register(
            "UpdateCommand",
            typeof(ICommand),
            typeof(EditableLabeledTextLine),
            new UIPropertyMetadata(null)
        );


        public ICommand UpdateCommand
        {
            get { return (ICommand)GetValue(UpdateCommandProperty); }
            set
            {
                SetValue(UpdateCommandProperty, value);
            }
        }

        public EditableLabeledTextLine()
        {
            InitializeComponent();
        }

        private void ShowEditingPanel(object sender, RoutedEventArgs e)
        {
            IsEditing = true;
        }

        private void CancelEditing(object sender, RoutedEventArgs e)
        {
            IsEditing = false;
        }
    }
}
