﻿using System;
using System.Windows.Data;

namespace NewAuctionManager.Converters
{
    [ValueConversion(typeof(string), typeof(string))]
    public class SplitThousandsTextConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (targetType != typeof(string))
                throw new InvalidOperationException("The target must be a boolean");

            string str = value.ToString(); string result = ""; string decimalGroup = "";
            int indexOfDecimlSeparator = str.IndexOf(culture.NumberFormat.CurrencyGroupSeparator);
            if (indexOfDecimlSeparator != -1)
            {
                decimalGroup = str.Substring(indexOfDecimlSeparator);
                str = str.Substring(0, indexOfDecimlSeparator);
            }
            int groups = str.Length / 3; int outOfGroup = str.Length - (groups * 3);
            result += str.Substring(0, outOfGroup) + " ";
            for(int i = 0; i < groups; ++i)
            {
                result += str.Substring(outOfGroup + i * 3, 3) + " ";
            }
            result = result.Trim();
            result += decimalGroup;
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            string str = value.ToString();
            return str?.Replace(" ", "");
        }

        #endregion
    }
}
