﻿using System;
using System.Collections.ObjectModel;
using SQLite;

namespace NewAuctionManager.Models
{
    public class AuctionModel
    {
        [NotNull]
        public string Name { get; set; }
        [NotNull, PrimaryKey]
        public string UID { get; set; }
        public string Content { get; set; }
        [NotNull]
        public DateTime PlacedDate { get; set; }
        [NotNull]
        public DateTime AcceptFirstPartBeforeDateTime { get; set; }
        [NotNull]
        public DateTime FirstPartCheckDate { get; set; }
        [NotNull]
        public DateTime AuctionDateTime { get; set; }
        [NotNull]
        public double Price { get; set; }
        [Ignore]
        public bool IsCancelled { get; set; } = false;
        public double ApplicationGuarantee { get; set; }
        public double ContractGuarantee { get; set; }
        public string PlaceOfDelivery { get; set; }
        public string AuctionOwner { get; set; }
        public bool IsInUse { get; set; }
        public DateTime LastUpdateDateTime { get; set; }

        public AuctionModel()
        {
            Notifications = new ObservableCollection<TenderNotificationModel>();
            Documents = new ObservableCollection<DocumentModel>();
            LocalDocuments = new ObservableCollection<LocalDocumentModel>();
            Clarifications = new ObservableCollection<ClarificationModel>();
        }

        [Ignore]
        public ObservableCollection<TenderNotificationModel> Notifications { get; set;}
        public string Link { get; set; }
        [Ignore]
        public ObservableCollection<DocumentModel> Documents { get; set; }
        [Ignore]
        public ObservableCollection<LocalDocumentModel> LocalDocuments { get; set; }
        [Ignore]
        public ObservableCollection<ClarificationModel> Clarifications { get; set; }
    }
}
