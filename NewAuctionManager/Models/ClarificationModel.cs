﻿using SQLite;
using System;

namespace NewAuctionManager.Models
{
    public class ClarificationModel
    {
        [PrimaryKey, Unique]
        public string UID { get; set; }
        [Indexed, NotNull]
        public string AuctionID { get; set; }
        public string DocumentUID{ get; set; }
        [NotNull]
        public DateTime PlacedDate { get; set; }
        public string Question { get; set; }
        public string Topic { get; set; }
        [Ignore]
        public DocumentModel Attachment { get; set; }
    }
}
