﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewAuctionManager.Models
{
    public class DocumentsModel
    {
        public static DocumentModel GetDocumentByUID(string uid)
        {
            return new DocumentModel() { UID = uid };
        }
    }
}
