﻿using System;
using SQLite;

namespace NewAuctionManager.Models
{
    public class TenderNotificationModel
    {
        [NotNull]
        public string FtpFileName { get; set; }
        [NotNull]
        public string FileName { get; set; }
        [PrimaryKey, Unique]
        public string UID { get; set; }
        [Indexed, NotNull]
        public string AuctionID { get; set; }
        [NotNull]
        public string Region { get; set; }
        [NotNull]
        public DateTime DatePlaced { get; set; }
    }
}
