﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewAuctionManager.Models
{
    public class LocalDocumentModel : DocumentModel
    {        
        public string LocalFileName { get; set; }
        public DateTime LocalUpdateDate { get; set; }
    }
}
