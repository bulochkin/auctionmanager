﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace NewAuctionManager.Models
{
    public class DocumentModel
    {
        [Unique, PrimaryKey]
        public string UID { get; set; }
        [NotNull]
        public string Name { get; set; }
        [NotNull]
        public string Link { get; set; }
        [NotNull]
        public string Size { get; set; }
        [NotNull]
        public string Description { get; set; }
        [NotNull, Indexed]
        public string AuctionID { get; set; }
        public bool IsActive { get; set; }
    }
}
