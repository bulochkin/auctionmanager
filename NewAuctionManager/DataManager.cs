﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Xml.Linq;
using ArxOne.Ftp;
using NewAuctionManager.Models;
using Ionic.Zip;
using SQLite;
using NewAuctionManager.ViewModels;

namespace NewAuctionManager
{
    class DataManager
    {
        public static void InitDatabase()
        {
            using (var db = GetWritableDatabase())
            {
                db.CreateTable<AuctionModel>();
                db.CreateTable<DocumentModel>();
                db.CreateTable<LocalDocumentModel>();
                db.CreateTable<TenderNotificationModel>();
                db.CreateTable<ClarificatonInfoModel>();
                db.CreateTable<ClarificationModel>();
            }
        }

        public static List<string> Regions = new List<string>() { "Chuvashskaja_Resp", "Marij_El_Resp", "Mordovija_Resp", "Moskovskaja_obl", "Moskva", "Nizhegorodskaja_obl", "Samarskaja_obl", "Saratovskaja_obl", "Tatarstan_Resp", "Uljanovskaja_obl", "Tverskaja_obl" };

        public static SQLiteConnection GetReadableDatabase()
        {
            var con = new SQLiteConnection("db.db", SQLiteOpenFlags.ReadOnly | SQLiteOpenFlags.NoMutex | SQLiteOpenFlags.ProtectionNone | SQLiteOpenFlags.SharedCache);
            try
            {
                var cur = con.Execute("SELECT COUNT(*) FROM sqlite_master WHERE type = ? AND name = ?", new String[] { "table", "AuctionModel" });
                if (cur != 1)
                {
                    InitDatabase();
                }
            }
            catch (SQLite.SQLiteException)
            {
                InitDatabase();
            }
            return con;
        }

        public static SQLiteConnection GetWritableDatabase()
        {
            return new SQLiteConnection("db.db", SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create | SQLiteOpenFlags.FullMutex | SQLiteOpenFlags.ProtectionComplete);
        }

        public async static Task UpdateDatabase(IProgress<int> progress = null, bool fullUpdate = false)
        {
            DateTime lastChecked = DateTime.Now;
            using (var con = DataManager.GetReadableDatabase())
            {
                var dt = con.ExecuteScalar<DateTime>("select Max(DatePlaced) from TenderNotificationModel");
                if (dt != null)
                {
                    lastChecked = dt;
                }
            }
            var startFromDate = lastChecked.Date.Subtract(TimeSpan.FromDays(1));
            if (fullUpdate)
            {
                startFromDate -= TimeSpan.FromDays(startFromDate.Day);
                startFromDate = startFromDate.AddMonths(-1);
                await UpdateDatabase(startFromDate, progress);
                progress.Report(0);
                await UpdateDatabase(startFromDate, progress, "prevMonth");
            } else
            {
                await UpdateDatabase(startFromDate, progress);
            }
        }

        public async static Task UpdateDatabase(DateTime startFromDate, IProgress<int> progress = null, string month="currMonth")
        {
            
            var client = new FtpClient(new Uri("ftp://ftp.zakupki.gov.ru"), new NetworkCredential("free", "free"));
            var regionList = client.ListEntries("fcs_regions");            
            double percentPerRegion = 10000 / Regions.Count;
            int regionsCompleted = 0;
            int zipCompleted = 0;
            int notificationsCompleted = 0;
            foreach (var region in Regions)
            {
                if (!regionList.Any(entry => entry.Name == region))
                    continue;
                var dir = @"fcs_regions/" + region + @"/notifications/"+ month;
                var list = client.ListEntries(dir);
                if (!Directory.Exists("temp_data"))
                    Directory.CreateDirectory("temp_data");
                List<TenderNotificationModel> tenders = new List<TenderNotificationModel>();
                List<ClarificatonInfoModel> clarifications = new List<ClarificatonInfoModel>();
                double percentPerZip = percentPerRegion / list.Count();
                zipCompleted = 0;
                foreach (var ftpItem in list)
                {
                    if (ftpItem.Type != FtpEntryType.File)
                        continue;
                    DateTime fileDateTime;
                    var NameParts = ftpItem.Name.Split('_');
                    DateTime.TryParseExact(NameParts[NameParts.Count() - 3].Substring(0, 8), "yyyyMMdd",
                        new DateTimeFormatInfo(), DateTimeStyles.None, out fileDateTime);
                    if (fileDateTime < startFromDate)
                        continue;
                    var zipname = "temp_data" + Path.DirectorySeparatorChar + ftpItem.Name;
                    var str = client.Retr(dir + @"/" + ftpItem.Name);
                    File.WriteAllBytes(zipname, new BinaryReader(str).ReadBytes((int) ftpItem.Size));
                    ZipFile file = new ZipFile(zipname);
                    double percentPerNotification = percentPerZip / file.Entries.Count();
                    notificationsCompleted = 0;
                    foreach (var fileEntry in file.Entries)
                    {
                        if (fileEntry.IsDirectory)
                            continue;
                        var lst = fileEntry.FileName.Split('_');
                        if (lst.Length < 3)
                            continue;
                        var auctionID = lst[1];
                        var uid = lst[2].Replace(".xml", "");
                        if (fileEntry.FileName.ToLower().Contains("notification"))
                        {
                            TenderNotificationModel info = new TenderNotificationModel()
                            {
                                FtpFileName = dir + @"/" + ftpItem.Name,
                                FileName = fileEntry.FileName,
                                AuctionID = auctionID,
                                UID = uid,
                                DatePlaced = fileDateTime,
                                Region = region
                            };
                            tenders.Add(info);
                        }
                        else if (fileEntry.FileName.ToLower().Contains("clarification"))
                        {
                            ClarificatonInfoModel info = new ClarificatonInfoModel()
                            {
                                FtpFileName = dir + @"/" + ftpItem.Name,
                                FileName = fileEntry.FileName,
                                AuctionID = auctionID,
                                UID = uid,
                                DatePlaced = fileDateTime,
                                Region = region
                            };
                            clarifications.Add(info);
                        }
                        notificationsCompleted++;
                        if (progress != null)
                        {
                            progress.Report(
                                (int)
                                (regionsCompleted * percentPerRegion + zipCompleted * percentPerZip +
                                    notificationsCompleted * percentPerNotification));
                        }
                    }
                    file.Dispose();
                    File.Delete(zipname);
                    zipCompleted++;
                    if (progress != null)
                    {
                        progress.Report((int) (regionsCompleted * percentPerRegion + zipCompleted * percentPerZip));
                    }
                }
                using (var con = GetWritableDatabase())
                {
                    con.BeginTransaction();
                    foreach (var tenderInfo in tenders)
                    {
                        con.InsertOrReplace(tenderInfo);
                    }
                    foreach (var clarificationInfo in clarifications)
                    {
                        con.InsertOrReplace(clarificationInfo);
                    }
                    con.Commit();
                }
                regionsCompleted++;
                if (progress != null)
                {
                    progress.Report((int) (regionsCompleted * percentPerRegion));
                }
            }
        }

        internal static void UpdateAuction(AuctionModel auction)
        {
            using (var con  = GetWritableDatabase())
            {
                con.InsertOrReplace(auction);
            }
        }

        public static List<TenderNotificationModel> GetNotificationsForAuction(string auctionID)
        {
            using (var con = DataManager.GetReadableDatabase())
            {
                var notifications = con.Table<TenderNotificationModel>().Where(t => t.AuctionID.Equals(auctionID));
                return notifications.ToList();
            }
        }

        public static List<ClarificationModel> GetClarificationsForAuction(string auctionID)
        {
            var clarificationsOnPortal = new List<ClarificatonInfoModel>();
            var clarificationsInDatabase = new List<ClarificationModel>();
            var clarificationsToAdd = new List<ClarificationModel>();
            var clarificationsToParse = new List<ClarificatonInfoModel>();
            using (var con = DataManager.GetReadableDatabase())
            {
                clarificationsOnPortal = con.Table<ClarificatonInfoModel>().Where(t => t.AuctionID.Equals(auctionID)).ToList();
                clarificationsInDatabase = con.Table<ClarificationModel>().Where(t => t.AuctionID.Equals(auctionID)).ToList();
                foreach(var cl in clarificationsInDatabase)
                {
                    cl.Attachment = con.Table<DocumentModel>().FirstOrDefault(doc => doc.UID == cl.DocumentUID);
                }
            }
            clarificationsToParse = clarificationsOnPortal.Where(c => !clarificationsInDatabase.Any(cc => cc.AuctionID == c.AuctionID)).ToList();
            foreach (var clarification in clarificationsToParse)
            {
                var path = DownloadNotification(clarification);
                var cl = GetClarificationFromFile(path);
                clarificationsToAdd.Add(cl);
                clarificationsInDatabase.Add(cl);
            }
            using (var con = DataManager.GetWritableDatabase())
            {
                con.BeginTransaction();
                foreach (var clarification in clarificationsToAdd)
                {
                    con.InsertOrReplace(clarification);
                }
                con.Commit();
            }
            return clarificationsInDatabase;
        }

        public static List<DocumentModel> GetDocumentsForAuction(string auctionID)
        {
            using (var con = DataManager.GetReadableDatabase())
            {
                var documents = con.Table<DocumentModel>().Where(t => t.AuctionID.Equals(auctionID) && t.IsActive == true);
                return documents.ToList();
            }
        }

        public static ClarificationModel GetClarificationFromFile(string path)
        {
            var result = new ClarificationModel();

            XDocument doc = XDocument.Load(path);
            var info = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}id").FirstOrDefault()?.Value;
            result.UID = info;
            info = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}purchaseNumber").FirstOrDefault()?.Value;
            result.AuctionID = info;
            info = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}publishedContentId").FirstOrDefault()?.Value;
            result.DocumentUID = info;
            info = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}docPublishDate").FirstOrDefault()?.Value;
            DateTime dt;
            DateTime.TryParse(info, out dt);
            result.PlacedDate = dt;
            info = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}question").FirstOrDefault()?.Value;
            result.Question = info;
            info = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}topic").FirstOrDefault()?.Value;
            result.Topic = info;
            var document = new DocumentModel();
            info = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}publishedContentId").FirstOrDefault()?.Value;
            document.UID = info;
            info = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}fileName").FirstOrDefault()?.Value;
            document.Name = info;
            info = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}url").FirstOrDefault()?.Value;
            document.Link = info;
            info = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}fileSize").FirstOrDefault()?.Value;
            document.Size = info;
            info = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}docDescription").FirstOrDefault()?.Value;
            document.Description = info;            
            document.AuctionID = "clarification" + result.UID;
            result.Attachment = document;
            File.Delete(path);

            using (var con = GetWritableDatabase())
            {
                con.InsertOrReplace(document);
            }

            return result;
        }

        public static string DownloadNotification(TenderNotificationModel notification)
        {
            var cacheDir = "cache";
            if (!Directory.Exists(cacheDir))
                Directory.CreateDirectory(cacheDir);
            var client = new FtpClient(new Uri("ftp://ftp.zakupki.gov.ru"), new NetworkCredential("free", "free"));
            string filename = notification.FtpFileName;
            var lst = client.ListEntries(filename.Substring(0, filename.LastIndexOf("/")));
            if (!lst.Any(item => item.Name == Path.GetFileName(filename)))
            {
                var startDate = notification.DatePlaced.Date;
                var endDate = notification.DatePlaced.AddDays(1).Date;
                var month = "";
                if (notification.DatePlaced.Month == DateTime.Now.Month ||
                    notification.DatePlaced.AddDays(1).Month == DateTime.Now.Month)
                {
                    month = "currMonth";
                }
                else if (notification.DatePlaced.Month == DateTime.Now.Month - 1)
                {
                    month = "prevMonth";
                }
                else
                {
                    startDate = startDate.Subtract(TimeSpan.FromDays(startDate.Day - 1));
                    endDate = endDate.Subtract(TimeSpan.FromDays(endDate.Day - 1)).AddMonths(1);
                }

                var path = "fcs_regions/" + notification.Region + "/notifications/" + month + "/";

                filename = "notification_" + notification.Region + "_" + startDate.ToString("yyyyMMdd") + "00_" +
                           endDate.ToString("yyyyMMdd") + "00_001.xml.zip";
                filename = path + filename;
            }
            int i = 1;
            bool cached = true;
            while (true)
            {
                filename = filename.Substring(0, filename.Length - 11) + i.ToString("D3") + ".xml.zip";
                string zipname = Path.Combine(cacheDir, Path.GetFileName(filename));
                if (!File.Exists(zipname))
                {
                    var str = client.Retr(filename);
                    if (str == null)
                        return null;
                    using (var memstr = new MemoryStream())
                    {
                        var r = new BinaryReader(str);
                        int read;

                        int size = 0;
                        byte[] buffer = new byte[16 * 1024];
                        while ((read = r.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            memstr.Write(buffer, 0, read);
                            size += read;
                        }
                        if (size < 3 * 1024 * 1024)
                        {
                            cached = false;
                            zipname = Path.GetTempFileName();
                        }
                        File.WriteAllBytes(zipname, memstr.ToArray());
                    }
                }
                ZipFile file = new ZipFile(zipname);
                var entry = file.FirstOrDefault(f => f.FileName == notification.FileName);
                if (entry == null)
                {
                    file.Dispose();
                    i++;
                    continue;
                }
                notification.FtpFileName = filename;
                using (var db = GetWritableDatabase())
                {
                    db.InsertOrReplace(notification);
                }
                var extractedName = Path.Combine(Path.GetTempPath(), entry.FileName);
                entry.Extract(Path.GetTempPath(), ExtractExistingFileAction.OverwriteSilently);
                file.Dispose();
                if (!cached)
                    File.Delete(zipname);
                return extractedName;
            }
        }

        public static AuctionModel GetAuctionById(string uid, bool forceUpdate = false)
        {
            if (String.IsNullOrWhiteSpace(uid))
                return null;
            AuctionModel result = null;
            using (var con = GetReadableDatabase())
            {
                result = con.Table<AuctionModel>().FirstOrDefault(auc => auc.UID == uid);
            }
            
            var notifications = DataManager.GetNotificationsForAuction(uid);            
            
            var notification = notifications.OrderByDescending(n => n.DatePlaced).FirstOrDefault();
            if (notification == null)
            {
                return null;
            }
            if (result == null || result.LastUpdateDateTime < notification.DatePlaced || forceUpdate)
            {
                result = new AuctionModel();
                if (notification.FileName.ToLower().Contains("cancel"))
                {
                    result.Name = uid + " - Аукцион отменен!";
                    result.IsCancelled = true;
                    return result;
                }
                result.IsInUse = false;
                var file = DataManager.DownloadNotification(notification);
                XDocument doc = XDocument.Load(file);
                var info = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}purchaseObjectInfo").FirstOrDefault()?.Value;
                result.Name = info;
                result.Content = info;
                info = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}purchaseNumber").FirstOrDefault()?.Value;
                result.UID = info;
                info = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}href").FirstOrDefault()?.Value;
                result.Link = info;
                info = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}customer")?.Elements("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}fullName")?.FirstOrDefault()?.Value;
                result.AuctionOwner = info;
                info = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}deliveryPlace")?.FirstOrDefault()?.Value;
                result.PlaceOfDelivery = info;
                info = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}lot")?.Elements("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}maxPrice")?.FirstOrDefault()?.Value;
                double tmp;
                double.TryParse(info, NumberStyles.Float, CultureInfo.GetCultureInfo("en-US"), out tmp);
                result.Price = tmp;
                info = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}contractGuarantee")?.Elements("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}amount")?.FirstOrDefault()?.Value;
                double.TryParse(info, NumberStyles.Float, CultureInfo.GetCultureInfo("en-US"), out tmp);
                result.ContractGuarantee = tmp;
                info = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}applicationGuarantee")?.Elements("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}amount")?.FirstOrDefault()?.Value;
                double.TryParse(info, NumberStyles.Float, CultureInfo.GetCultureInfo("en-US"), out tmp);
                result.ApplicationGuarantee = tmp;
                DateTime dt;
                info = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}collecting")?.Elements("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}startDate")?.FirstOrDefault()?.Value;
                DateTime.TryParse(info, out dt);
                result.PlacedDate = dt;
                info = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}collecting")?.Elements("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}endDate")?.FirstOrDefault()?.Value;
                DateTime.TryParse(info, out dt);
                result.AcceptFirstPartBeforeDateTime = dt;
                info = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}scoring")?.Elements("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}date")?.FirstOrDefault()?.Value;
                DateTime.TryParse(info, out dt);
                result.FirstPartCheckDate = dt;
                info = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}bidding")?.Elements("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}date")?.FirstOrDefault()?.Value;
                DateTime.TryParse(info, out dt);
                result.AuctionDateTime = dt;

                var atts = doc.Descendants("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}attachment");
                DocumentModel docum;
                var db = DataManager.GetWritableDatabase();
                db.BeginTransaction();
                var docs = GetDocumentsForAuction(result.UID);
                foreach (var document in docs)
                {
                    document.IsActive = false;
                    db.InsertOrReplace(document);
                }
                foreach (var att in atts)
                {
                    docum = new DocumentModel();
                    info = att.Elements("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}publishedContentId")?.FirstOrDefault()?.Value;
                    docum.UID = info;
                    info = att.Elements("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}fileName")?.FirstOrDefault()?.Value;
                    docum.Name = info;
                    info = att.Elements("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}url")?.FirstOrDefault()?.Value;
                    docum.Link = info;
                    info = att.Elements("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}docDescription")?.FirstOrDefault()?.Value;
                    docum.Description = info;
                    info = att.Elements("{" + doc.Root.GetDefaultNamespace().NamespaceName + "}fileSize")?.FirstOrDefault()?.Value;
                    docum.Size = info;
                    docum.AuctionID = result.UID;
                    docum.IsActive = true;
                    result.Documents.Add(docum);
                    db.InsertOrReplace(docum);
                }
                db.Commit();
                db.InsertOrReplace(result);
                db.Dispose();

                result.LastUpdateDateTime = notification.DatePlaced;
                File.Delete(file);
            }
            else
            {                
                var documents = DataManager.GetDocumentsForAuction(result.UID);
                foreach(var document in documents)
                {
                    result.Documents.Add(document);
                }
            }
            var clarifications = DataManager.GetClarificationsForAuction(result.UID);
            foreach (var clarification in clarifications)
            {
                result.Clarifications.Add(clarification);
            }
            foreach (var n in notifications)
                result.Notifications.Add(n);
            return result;
        }

        public static List<AuctionModel> GetActiveAuctions()
        {            
            var auctions = DataManager.GetReadableDatabase().Query<AuctionModel>("select * from AuctionModel where IsInUse='1' ");
            var result = new List<AuctionModel>();
            foreach (var auc in auctions)
            {
                var auction = GetAuctionById(auc.UID);
                if (auction.IsInUse != auc.IsInUse)
                {
                    auction.IsInUse = auc.IsInUse;
                    UpdateAuction(auction);
                }
                result.Add(auction);
            }
            return result;
        }
    }
}
