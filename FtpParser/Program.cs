﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using ArxOne.Ftp;
using Ionic.Zip;
using SQLite;

namespace FtpParser
{
    class TenderNotificationModel
    {
        [NotNull]
        public string FtpFileName { get; set; }
        [NotNull]
        public string FileName { get; set; }
        [PrimaryKey, Unique]
        public string UID { get; set; }
        [Indexed, NotNull]
        public string AuctionID { get; set; }
        [NotNull]
        public string Region { get; set; }
        [NotNull]
        public DateTime DatePlaced { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Ftpob(new DateTime(2016, 12, 01));//Ftpob(DateTime.Now.Date - TimeSpan.FromDays(1));
        }

        static void Ftpob(DateTime startFromDate)
        {
            var client = new FtpClient(new Uri("ftp://ftp.zakupki.gov.ru"), new NetworkCredential("free", "free"));
            Console.WriteLine("Подключение к ftp серверу");
            var list = client.ListEntries("fcs_regions");
            int res = -1;
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Выберите регион:");
                int i = 0;
                foreach (var ftpItem in list)
                {
                    if (ftpItem.Type == FtpEntryType.Directory)
                    {
                        Console.WriteLine("[" + i + "] " + ftpItem.Name);
                    }
                    i++;
                }
                int.TryParse(Console.ReadLine(), out res);
                if (res == -1)
                    Console.WriteLine("Неправильный регион, попробуйте еще раз");
                else
                    break;
            }
            var region = list.ElementAt(res).Name;
            var dir = @"fcs_regions/" + region + @"/notifications/prevMonth";
            list = client.ListEntries(dir);
            if (!Directory.Exists("temp_data"))
                Directory.CreateDirectory("temp_data");
            List<TenderNotificationModel> tenders = new List<TenderNotificationModel>();
            Console.WriteLine("Обработка данных");
            foreach (var ftpItem in list)
            {
                if (ftpItem.Type != FtpEntryType.File)
                    continue;
                if (!ftpItem.Name.Contains("001.xml"))
                    continue;
                DateTime fileDateTime;
                DateTime.TryParseExact(ftpItem.Name.Split('_')[3].Substring(0, 8), "yyyyMMdd", new DateTimeFormatInfo(), DateTimeStyles.None, out fileDateTime);
                if (fileDateTime < startFromDate)
                    continue;
                Console.WriteLine("* " + ftpItem.Name);
                var zipname = "temp_data" + Path.DirectorySeparatorChar + ftpItem.Name;
                var str = client.Retr(dir + @"/" + ftpItem.Name);
                File.WriteAllBytes(zipname, new BinaryReader(str).ReadBytes((int)ftpItem.Size));
                ZipFile file = new ZipFile(zipname);
                foreach (var fileEntry in file.Entries)
                {
                    Console.WriteLine("** " + fileEntry.FileName);
                    if (fileEntry.IsDirectory)
                        continue;
                    var lst = fileEntry.FileName.Split('_');
                    if (lst.Length < 3)
                        continue;
                    var auctionID = lst[1];
                    var uid = lst[2].Replace(".xml", "");
                    TenderNotificationModel info = new TenderNotificationModel()
                    {
                        FtpFileName = dir + @"/" + ftpItem.Name,
                        FileName = fileEntry.FileName,
                        AuctionID = auctionID,
                        UID = uid,
                        DatePlaced = fileDateTime,
                        Region = region
                    };
                    tenders.Add(info);
                }
                file.Dispose();
                File.Delete(zipname);
            }
            var con = new SQLiteConnection("db.db", SQLiteOpenFlags.Create | SQLiteOpenFlags.ReadWrite);
            con.CreateTable<TenderNotificationModel>();
            con.BeginTransaction();
            foreach (var tenderInfo in tenders)
            {
                con.InsertOrReplace(tenderInfo);
            }
            con.Commit();
            con.Dispose();
            Console.WriteLine("Всего обработано: " + tenders.Count + " закупок.");
            Console.ReadLine();
        }
    }
}
